import { Component } from "react";
import likeImage from '../../../assets/images/like-yellow-icon.png';

class OutputContent extends Component {
    render() {
        return (
            <>
                <div className='col-12 mt-3'>
                    <p>Nội dung thông điệp</p>
                </div>
                <div className='col-12 mt-3'>
                    <img src={likeImage} width="100px" />
                </div>    
            </>
        )
    }
}

export default OutputContent