import { Component } from "react";
import InputContent from "./input/InputContent";
import OutputContent from "./output/OutputContent";

class ContentComponent extends Component {
    render() {
        return (
            <>
                <InputContent/>
                <OutputContent/>
            </>
        )
    }
}

export default ContentComponent