import { Component } from "react";

class InputContent extends Component {
    handleInputChange(event) {       
        console.log("Input Change ....");
        console.log(event.target.value);
    }

    handleButtonClick() {
        console.log("Button click ...");
    }

    render() {
        return (
            <>
                <div className='col-12 mt-3'>
                    <label>Message cho bạn 12 tháng tới</label>
                </div>
                <div className='col-12 mt-3'>
                    <input className='form-control' onChange={this.handleInputChange} placeholder='nhập vào message'/>
                </div>
                <div className='col-12 mt-3'>
                    <button className='btn btn-primary' onClick={this.handleButtonClick}>Gửi thông điệp</button>
                </div>
            </>
        )
    }
}

export default InputContent