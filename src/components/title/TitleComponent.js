import { Component } from "react";
import ImageTitle from "./image/ImageTitle";
import TextTitle from "./text/TextTitle";

class TitleComponent extends Component {
    render() {
        return (
            <>
                <TextTitle/>
                <ImageTitle/>
            </>
        )
    }
}

export default TitleComponent